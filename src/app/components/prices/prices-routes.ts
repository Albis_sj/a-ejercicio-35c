import { Routes } from "@angular/router";
import { days_Routes } from "./primer-price-routes";
import { PrimerPriceComponent } from "./primer-price.component";
import { SegundoPriceComponent } from "./segundo-price.component";
import { TercerPriceComponent } from "./tercer-price.component";


export const Usuario_Routes: Routes =[
  { path: '20/:id', 
  component: PrimerPriceComponent,
  children:  days_Routes,},
  { path: '50', component: SegundoPriceComponent },
  { path: '80', component: TercerPriceComponent },
  { path: '**', pathMatch: 'full', redirectTo: '20' }
]
