import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { PricesComponent } from './components/prices/prices.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SegundoPriceComponent } from './components/prices/segundo-price.component';
import { TercerPriceComponent } from './components/prices/tercer-price.component';
import { PrimerPriceComponent } from './components/prices/primer-price.component';
import { PrimerSundayComponent } from './components/prices/primer-sunday.component';
import { PrimerMondayComponent } from './components/prices/primer-monday.component';
import { PrimerTuesdayComponent } from './components/prices/primer-tuesday.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    HomeComponent,
    PricesComponent,
    SidebarComponent,
    PrimerPriceComponent,
    SegundoPriceComponent,
    TercerPriceComponent,
    PrimerSundayComponent,
    PrimerMondayComponent,
    PrimerTuesdayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
