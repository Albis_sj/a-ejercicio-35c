import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimerSundayComponent } from './primer-sunday.component';

describe('PrimerSundayComponent', () => {
  let component: PrimerSundayComponent;
  let fixture: ComponentFixture<PrimerSundayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimerSundayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimerSundayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
