import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimerTuesdayComponent } from './primer-tuesday.component';

describe('PrimerTuesdayComponent', () => {
  let component: PrimerTuesdayComponent;
  let fixture: ComponentFixture<PrimerTuesdayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimerTuesdayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimerTuesdayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
