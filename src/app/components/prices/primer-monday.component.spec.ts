import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimerMondayComponent } from './primer-monday.component';

describe('PrimerMondayComponent', () => {
  let component: PrimerMondayComponent;
  let fixture: ComponentFixture<PrimerMondayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimerMondayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimerMondayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
